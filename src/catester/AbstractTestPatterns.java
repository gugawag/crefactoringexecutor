package catester;

/**
 * Interface que representa nomes de padr�es para gera��o de testes de diferentes frameworks de testes para C.
 * @author gugawag
 *
 */
public abstract class AbstractTestPatterns {

	public String getPadraoNomeFuncao(){
		return "#nomeFuncao#";
	}
	public String getPadraoExpressao(){
		return "#expressao#";
	}
	
	public String getPadraoCabecalhoGeral(){
		return "#include <stdio.h>\n#include <stdlib.h>\n#include <string.h>\n#include <stdbool.h>\n";
	}
	
	public String getPadraoMain(){
		return "int main(void){\n";
	}
	
	public String getPadraoNomeVariavel(){
		return "#nomeVariavel#";
	}
	
	public String getPadraoNumeroTeste(){
		return "#numero_teste#";
	}
	
	
	public String getPadraoDeclaracaoVariavel(String tipo){
		//� array
		if (tipo.contains("[]")){
			return tipo.substring(0, tipo.indexOf("[")) + " " + getPadraoNomeVariavel() +"[] = "+ getPadraoExpressao() + ";\n";
		}
		
		//� ponteiro
		if (tipo.contains("*")){
			return tipo.substring(0, tipo.indexOf("*")) + " *" + getPadraoNomeVariavel() +" = "+ getPadraoExpressao() + ";\n";
		}
		
		return tipo + " " + getPadraoNomeVariavel() +" = "+ getPadraoExpressao() + ";\n";
	}
	
	public String getPadraoDeclaracaoVariavelSemValor(String tipo){
		//� array
		if (tipo.contains("[]")){
			return tipo.substring(0, tipo.indexOf("[")) + " " + getPadraoNomeVariavel() +"[0];\n";
		}
		
		//� ponteiro
		if (tipo.contains("*")){
			return tipo.substring(0, tipo.indexOf("*")) + " *" + getPadraoNomeVariavel() +";\n";
		}

		
		return tipo + " " + getPadraoNomeVariavel() +";\n";
	}	
	
	public abstract String getFuncaoTeste();
	public abstract String getPadraoAssert();
	public abstract String getPadraoAssertTrue();
	public abstract String getPadraoAssertEquals();
	public abstract String getPadraoFail();
	public abstract String getPadraoPass();
	public abstract String getPadraoCabecalhoFWTestes();
	public String getPadraoFuncoesUtilitariasC() {
		return null;
	}

}

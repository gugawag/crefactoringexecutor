package catester;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class RandoopUnitPatterns extends AbstractTestPatterns {

	@Override
	public String getFuncaoTeste() {
		return "void test_#nomeFuncao#(void){\n";
	}

	@Override
	public String getPadraoAssert() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPadraoFail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPadraoPass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPadraoCabecalhoFWTestes() {
		return null;
	}

	@Override
	public String getPadraoAssertTrue() {
		return "asserttrue(#numero_teste#, #expressao#);";
	}

	@Override
	public String getPadraoAssertEquals() {
		return "assertequals(#expressao#, #expressao#);";
	}
	
	@Override
	public String getPadraoMain() {
		return super.getPadraoMain() + "cadastraSinais();\n" + 
				"entrada = fopen(\"dados.txt\", \"r\");\n" +
				"stdin = entrada;\n" +
				"saida = fopen(\"[nome_arquivo_saida]\", \"w\");\n" +
				"stdout = saida;\nstderr = saida;\n" +
				"oraculo = fopen(\"[nome_arquivo_oraculo]\", \"w\");\n";
	}
	
	@Override
	public String getPadraoFuncoesUtilitariasC(){
		return "#include \"funcoes.h\"\n\n";
	}

}


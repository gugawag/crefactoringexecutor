package br.edu.ufcg.ines.carefactor.refactorings;

import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.RefactoringStatusEntry;

public class Util {

	public static void mostraStatusRefactoring(RefactoringStatus status) {
        for (RefactoringStatusEntry entry:status.getEntries()){
        	System.err.println("Entrada: " + entry.getMessage());
        }
        System.err.println("tem erro? " + status.hasError());
        System.err.println("tem warning? " + status.hasWarning());
        System.err.println("tem info? " + status.hasInfo());
        System.err.println("tem erro fatal? " + status.hasFatalError());		
	}
	
	public static String getRefactoringStatusMessage(RefactoringStatus rs){
    	RefactoringStatusEntry[] rse = rs.getEntries();
    	StringBuffer strBuffer = new StringBuffer();
    	for (int i=0; i< rse.length; i++){
    		RefactoringStatusEntry entry = rse[i];
    		strBuffer.append(entry.getMessage());
    	} 
    	return strBuffer.toString();
	}

}

/*******************************************************************************
 * Copyright (c) 2008, 2012 Institute for Software, HSR Hochschule fuer Technik  
 * Rapperswil, University of applied sciences and others
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html  
 *  
 * Contributors: 
 *     Institute for Software - initial API and implementation
 *     Sergey Prigogin (Google)
 *******************************************************************************/
package br.edu.ufcg.ines.carefactor.refactorings;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.dom.ast.IASTName;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.cdt.internal.core.dom.parser.c.CNodeFactory;
import org.eclipse.cdt.internal.ui.refactoring.CRefactoringContext;
import org.eclipse.cdt.internal.ui.refactoring.MethodContext;
import org.eclipse.cdt.internal.ui.refactoring.MethodContext.ContextType;
import org.eclipse.cdt.internal.ui.refactoring.extractconstant.ExtractConstantRefactoring;
import org.eclipse.cdt.internal.ui.refactoring.utils.VisibilityEnum;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.RefactoringStatusEntry;

/**
 * Tests for Extract Constant refactoring.
 */
public class ExtractConstantRefactor {
	private String extractedConstantName = "EXTRACTED";
	private VisibilityEnum visibility = VisibilityEnum.v_private;
	private ExtractConstantRefactoring refactoring;
	
	private static Logger logger = Logger.getLogger(ExtractConstantRefactor.class);

	public ExtractConstantRefactor() {
		super();
	}

//	protected void simulateUserInput() {
//		ExtractConstantInfo info = refactoring.getRefactoringInfo();
//		info.setName(extractedConstantName);
//		info.setVisibility(visibility);
//	}
	
    public RefactoringStatus checkConditionsAndPerformRefactoring(IFile file, int offset, int length, String constName, ICProject project) throws Exception {
        refactoring = createRefactoring(file, offset, length, project);
        setRefactoringInfo(refactoring, constName);
        refactoring.setContext(new CRefactoringContext(refactoring));
        try {
        	return checkConditionsAndPerformRefactoring(refactoring);
        } finally {
        }
    }

	
    private void setRefactoringInfo(ExtractConstantRefactoring refactoring, String constName) {
        refactoring.getRefactoringInfo().setName(constName);
        refactoring.getRefactoringInfo().setVisibility(VisibilityEnum.v_private);
        MethodContext context = new MethodContext();
        context.setType(ContextType.NONE);
        CNodeFactory c = CNodeFactory.getDefault();
        IASTName n = c.newName(constName.toCharArray());
        context.setMethodDeclarationName(n);
        refactoring.getRefactoringInfo().setMethodContext(context);
	}

	//Analisa pr�-condi��es; realiza realmente o refactoring, alterando o(s) arquivo(s)
    private RefactoringStatus checkConditionsAndPerformRefactoring(ExtractConstantRefactoring proc) throws CoreException {
        RefactoringStatus rs =proc.checkInitialConditions(new NullProgressMonitor() );
        if (!rs.hasError()){
            rs= proc.checkFinalConditions(new NullProgressMonitor());
            //gerando a transforma��o
            Change change = proc.createChange(new NullProgressMonitor());
            //realmente refatorando
            change.perform(new NullProgressMonitor());
        }
        return rs;
    }
    
    
    private ExtractConstantRefactoring createRefactoring(IFile file, int offset, int length, ICProject project) {
    	ExtractConstantRefactoring proc= new ExtractConstantRefactoring((ITranslationUnit) CoreModel.getDefault().create(
				file), new TextSelection(offset, length), project);
    	
        return proc;
    }
    
    public RefactoringStatus checkConditions(IFile file, int offset, int length, String constName, ICProject project) throws Exception {
    	refactoring = createRefactoring(file, offset, length, project);
    	setRefactoringInfo(refactoring, constName);
    	refactoring.setContext(new CRefactoringContext(refactoring));
        RefactoringStatus rs = null;
        try{
        	rs =refactoring.checkInitialConditions(new NullProgressMonitor() );
	        if (!rs.hasError()){
	        		rs= refactoring.checkFinalConditions(new NullProgressMonitor());	
	        }
        } finally {
        }

        return rs;
    }
    
    public String getRefactorMessages(IFile file, int offset, int length, String constName, ICProject project) throws Exception {
    	refactoring = createRefactoring(file, offset, length, project);
    	setRefactoringInfo(refactoring, constName);
    	refactoring.setContext(new CRefactoringContext(refactoring));
        try {
        	RefactoringStatus rs = checkConditions(file, offset, length, constName, project);
        	if (!rs.hasWarning()){
        		logger.error("Input check on "+ constName + " passed. There should have been warnings or errors. ") ; //$NON-NLS-1$ //$NON-NLS-2$
        		return null;
        	}
        	RefactoringStatusEntry[] rse = rs.getEntries();
        	StringBuffer strBuffer = new StringBuffer();
        	for (int i=0; i< rse.length; i++){
        		RefactoringStatusEntry entry = rse[i];
        		strBuffer.append(entry.getMessage());
        	} 
        	return strBuffer.toString();
        } finally {
        }
    }
}

/*******************************************************************************
 * Copyright (c) 2005, 2010 Wind River Systems, Inc.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution, and is available at 
 * http://www.eclipse.org/legal/epl-v10.html  
 * 
 * Contributors: 
 *     Markus Schorn - initial API and implementation 
 ******************************************************************************/ 
package br.edu.ufcg.ines.carefactor.refactorings;

import org.apache.log4j.Logger;
import org.eclipse.cdt.internal.ui.refactoring.rename.CRefactoringArgument;
import org.eclipse.cdt.internal.ui.refactoring.rename.CRefactory;
import org.eclipse.cdt.internal.ui.refactoring.rename.CRenameProcessor;
import org.eclipse.cdt.internal.ui.refactoring.rename.CRenameRefactoring;
import org.eclipse.cdt.internal.ui.refactoring.rename.TextSearchWrapper;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.RefactoringStatusEntry;

/**
 * @author markus.schorn@windriver.com
 */
public class RenameRefactor{
    private static final IProgressMonitor NPM = new NullProgressMonitor();

    private static Logger logger = Logger.getLogger(RenameRefactor.class);
    
    public RenameRefactor() {
    }

    /**
     * @param element   The CElement to rename
     * @param newName   The new name for the element
     * @return
     * @throws Exception
     */
    public Change getRefactorChanges(IFile file, int offset, String newName) throws Exception {
        CRenameRefactoring proc = createRefactoring(file, offset, newName);
        
        ((CRenameProcessor) proc.getProcessor()).lockIndex();
        try {
        	RefactoringStatus rs = checkConditionsAndPerformRefactoring(proc);
        	if (!rs.hasError()) {
        		Change change = proc.createChange( new NullProgressMonitor() );
        		return change;
        	} 

        	logger.error("Input check on " + newName + " failed. "+rs.getEntryMatchingSeverity(RefactoringStatus.ERROR) ); //$NON-NLS-1$ //$NON-NLS-2$
        	//rs.getFirstMessage(RefactoringStatus.ERROR) is not the message displayed in 
        	//the UI for renaming a method to a constructor, the first message which is only
        	//a warning is shown in the UI. If you click preview, then the error and the warning
        	//is shown. 
        	return null;
        } finally {
            ((CRenameProcessor) proc.getProcessor()).unlockIndex();
        }
    }

    private CRenameRefactoring createRefactoring(IFile file, int offset, String newName) {
    	CRefactoringArgument arg= new CRefactoringArgument(file, offset, 0);
        CRenameProcessor proc= new CRenameProcessor(CRefactory.getInstance(), arg);
        proc.setReplacementText( newName );
        proc.setSelectedOptions(-1);
        //Gustavo comentei o de baixo e acrescentei o SCOPE_FILE
//        proc.setExhaustiveSearchScope(TextSearchWrapper.SCOPE_WORKSPACE);
        proc.setExhaustiveSearchScope(TextSearchWrapper.SCOPE_FILE);
        
        return new CRenameRefactoring(proc);
    }

    public String getRefactorMessages(IFile file, int offset, String newName) throws Exception {
        CRenameRefactoring proc = createRefactoring(file, offset, newName);
        ((CRenameProcessor) proc.getProcessor()).lockIndex();
        try {
        	RefactoringStatus rs = checkConditions(file,offset, newName);
        	RefactoringStatusEntry[] rse = rs.getEntries();
        	StringBuffer strBuffer = new StringBuffer();
        	for (int i=0; i< rse.length; i++){
        		RefactoringStatusEntry entry = rse[i];
        		strBuffer.append(entry.getMessage());
        	} 
        	return strBuffer.toString();
        } finally {
            ((CRenameProcessor) proc.getProcessor()).unlockIndex();
        }
    }

    public RefactoringStatus checkConditionsAndPerformRefactoring(IFile file, int offset, String newName) throws Exception {
        CRenameRefactoring proc = createRefactoring(file, offset, newName);
        ((CRenameProcessor) proc.getProcessor()).lockIndex();
        
        try {
        	return checkConditionsAndPerformRefactoring(proc);
        } finally {
            ((CRenameProcessor) proc.getProcessor()).unlockIndex();
        }
    }
    
    //Analisa pr�-condi��es; realiza realmente o refactoring, alterando o(s) arquivo(s)
    private RefactoringStatus checkConditionsAndPerformRefactoring(CRenameRefactoring proc) throws CoreException {
        RefactoringStatus rs =proc.checkInitialConditions(new NullProgressMonitor() );
        if (!rs.hasError()){
            rs= proc.checkFinalConditions(new NullProgressMonitor());
            //gerando a transforma��o
            Change change = proc.createChange(new NullProgressMonitor());
            //realmente refatorando
            change.perform(new NullProgressMonitor());
        }
        return rs;
    }
    
    //Analisa pr�-condi��es; 
    public RefactoringStatus checkConditions(IFile file, int offset, String newName) throws Exception {
    	CRenameRefactoring proc = createRefactoring(file, offset, newName);
        ((CRenameProcessor) proc.getProcessor()).lockIndex();
        RefactoringStatus rs = null;
        try{
        	rs =proc.checkInitialConditions(new NullProgressMonitor() );
	        if (!rs.hasError()){
	        	((CRenameProcessor) proc.getProcessor()).lockIndex();
	        		rs= proc.checkFinalConditions(new NullProgressMonitor());	
	        }
        } finally {
            ((CRenameProcessor) proc.getProcessor()).unlockIndex();
        }

        return rs;
    }

    /**
     * FIXME n�o sei porque, mas se for executado o c�digo abaixo est� dando nullpointer quando se faz o createChange
     * Prepara e executa a transforma��o. Altera efetivamente o arquivo.
     * @param proc
     * @return
     * @throws CoreException
     */
    public Change performRefactoring(IFile file, int offset, String newName) throws CoreException {
    	CRenameRefactoring proc = createRefactoring(file, offset, newName);
    	
        //prepara a transforma��o
        Change change = proc.createChange(NPM);
        
        //aplica a transforma��o
        change.perform(NPM);
        
        return change;
    }

    public int getRefactorSeverity(IFile file, int offset, String newName) throws Exception {
        CRenameRefactoring proc = createRefactoring(file, offset, newName);
        ((CRenameProcessor) proc.getProcessor()).lockIndex();
        try {
        	RefactoringStatus rs = checkConditionsAndPerformRefactoring(proc);
        	return rs.getSeverity();
        } finally {
            ((CRenameProcessor) proc.getProcessor()).unlockIndex();
        }
    }

    protected int countOccurrences(String contents, String lookup) {
        int idx= contents.indexOf(lookup);
        int count= 0;
        while (idx >= 0) {
            count++;
            idx= contents.indexOf(lookup, idx+lookup.length());
        }
        return count;
    }
    

}

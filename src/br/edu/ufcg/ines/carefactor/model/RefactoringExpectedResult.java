package br.edu.ufcg.ines.carefactor.model;

/**
 * Represents a result of a refactoring, given the inputs.
 * @author Gustavo Wagner, gugawag@gmail.com
 *
 */
public class RefactoringExpectedResult {
	
	//the new name of the element under refactoring.
	private String newName;
	private boolean sucessRefactoring;
	
	private NewNameType newNameType;
	
	public enum NewNameType{
		KEYWORD, SPACED_NAME, POINTER_NAME, POINTER_ADRESS_NAME, NEW_NAME, EXISTING_GLOBAL_VAR_NAME, EXISTING_FUNCTION_NAME, LITERAL, EXISTING_DEFINE_NAME;
	}
	
	public RefactoringExpectedResult(String newName, NewNameType newNameType, boolean sucessRefactoring) {
		super();
		this.newName = newName;
		this.sucessRefactoring = sucessRefactoring;
		this.newNameType = newNameType;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	/** 
	 * @return True if it is expected that refactoring to the newName be success. False, otherwise.
	 */
	public boolean isSucessRefactoring() {
		return sucessRefactoring;
	}

	public void setSucessRefactoring(boolean sucessRefactoring) {
		this.sucessRefactoring = sucessRefactoring;
	}
	
	public NewNameType getNewNameType() {
		return newNameType;
	}

	public void setNewNameType(NewNameType newNameType) {
		this.newNameType = newNameType;
	}

}

package br.edu.ufcg.ines.carefactor.experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.model.CModelException;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringData;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.RefactoringType;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.ResultType;
import br.edu.ufcg.ines.carefactor.experiments.statistics.StatisticGenerator;
import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult;
import br.edu.ufcg.ines.carefactor.refactorings.RefactoringExecutor;
import br.edu.ufcg.ines.carefactor.refactorings.Util;
import br.edu.ufcg.ines.carefactor.util.Compile;
import br.edu.ufcg.ines.carefactor.util.FuncoesGerais;
import br.edu.ufcg.ines.carefactor.util.RegressionTestsResult;

public class ExecutorCompleto {

	private static Logger logger = Logger.getLogger(ExecutorCompleto.class);
	
	public static String workspacePath = "/Users/gugawag/Documents/mestrado/runtime-EclipseApplication/";
	public static String projectName = "ACExperiments";
	public static String srcFolderPrefix = "/src/experiment1/";
	public static String refactoredProgramFolder = srcFolderPrefix + "/carefactor/";
	public static String errorFolder = "errors";
	public static String infoFolder = "info";
	
	/*
	//just refactoring functions, local variable, global variable, function parameters; TODO constant?
	public void runRenameRefactoring(int cElementType, String oldName, String newName){
		StatisticGenerator stGen = StatisticGenerator.getInstance();
		logger.info("Starting rename local variable refactoring...");
		RefactoringExecutor rex = new RefactoringExecutor();
		int programNumber = 1;
		File programFolderFile = new File(workspacePath + projectName + srcFolderPrefix + programNumber);
		stGen.addRefactoringExecutions(String.valueOf(programNumber));
		RefactoringData refactoringData = stGen.getExecutionDataOfProgram(String.valueOf(programNumber));
		
		File resultsFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_rename-localvariables.properties");
		if (!resultsFile.exists()){
			resultsFile.mkdir();
		}
		
		PrintWriter resultsWriter = null;
		try {
			resultsWriter = new PrintWriter(resultsFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		while(programFolderFile.exists()){
			for (File programFile: programFolderFile.listFiles()){
				
				if (programFile.getName().endsWith(".c") && !programFile.getName().startsWith("test_") ){
					
					
					ITranslationUnit tu = FuncoesGerais.abreArquivo(projectName + "/" + srcFolderPrefix + "/" + programNumber + "/" + programFile.getName());
					
					List<RefactoringExpectedResult> refExpectResult = new ArrayList<RefactoringExpectedResult>();
					refExpectResult.add(new RefactoringExpectedResult(newName, false));
					
					System.out.println("Ser�o refatorados para nomes: ");
					for (RefactoringExpectedResult nome: refExpectResult){
						System.out.println(nome.getNewName());
					}

					//choosing the C elements to be refactored according to cElementType
					String refactoringFolder = null;
					RefactoringType refactoringType = null;
					switch(cElementType){
						case ICElement.C_FUNCTION:{
							refactoringFolder = refactoredProgramFolder + "/rename-function/";
							refactoringType = RefactoringType.RENAME_FUNCTION;
							break;
						}
						case ICElement.C_VARIABLE:{
							refactoringFolder = refactoredProgramFolder + "/rename-globalvariable/";
							refactoringType = RefactoringType.RENAME_GLOBAL_VARIABLE;
							break;
						}
						case ICElement.C_VARIABLE_LOCAL:{
							refactoringFolder = refactoredProgramFolder + "/rename-localvariable/";
							refactoringType = RefactoringType.RENAME_LOCAL_VARIABLE;
							break;
						}
					}
					
					//renaming
						//adding an unused name
						refExpectResult.add(new RefactoringExpectedResult(oldName + "_REFACTORED", true));
						for (RefactoringExpectedResult exceptedResult: refExpectResult){
							
							//statistics
							RefactoringResult refactoringResult = new RefactoringResult();
							refactoringData.addRefactoringResult(refactoringResult);
							refactoringResult.setExpectedResult(exceptedResult);
							refactoringResult.setRefactoringType(refactoringType.name());
							refactoringResult.setFileName(programFile.getName());
							refactoringResult.setOldName(oldName);
							refactoringResult.setNewName(exceptedResult.getNewName());
							
							try {
								long initialTime = (new Date()).getTime();
								//TODO apenas a linha abaixo est� espec�fica. Alter�-la para deixar gen�rica para qualquer tipo de rename 
								RefactoringStatus refactoringStatus = rex.runRenameRefactoring(srcFolderPrefix, programNumber, programFile.getName(), oldName, 
										exceptedResult.getNewName(), refactoringFolder, 1); //ie.: .../src/1/
								long finalTime = (new Date()).getTime();
								refactoringResult.setRefatoringExecutionTime(finalTime - initialTime);
								if (!refactoringStatus.hasError()){
									//trying to compile the program
									if (refactoringStatus.hasInfo() || refactoringStatus.hasWarning()){
										refactoringResult.setResult(ResultType.REFACTORING_INFO.name());										
									}
									
									String refactoredProgramFolder = workspacePath + "/" + projectName + "/" + refactoringFolder + "/" + programNumber + "/" + 
											oldName + "_" + exceptedResult.getNewName() + "/";
									logger.info("Refactored without error. Trying to compile in " + refactoredProgramFolder);
									boolean compiled = Compile.run(refactoredProgramFolder, true);
									
									//trying to execute regression test
									if (compiled){
										logger.info("Compiled!");
										
										//trying to execute regression tests
										FuncoesGerais.copyFilesToRegressionTests(programFolderFile.getAbsolutePath(), refactoredProgramFolder);
										//compile and run the regression tests
										boolean compiledAndRunnedRegressionTests = Compile.run(refactoredProgramFolder, false);
										if (compiledAndRunnedRegressionTests){
											RegressionTestsResult regressioTestsResult = FuncoesGerais.getRegressionTestsResult(refactoredProgramFolder);
											if (regressioTestsResult.isTestsPassed()){
												refactoringResult.setResult(ResultType.SUCCESS.name());
											}else {
												refactoringResult.setResult(ResultType.REGRESSION_TEST_FAIL.name());
												refactoringResult.setMessage(regressioTestsResult.getRegressionTestMessage());
											}
										} else{
											//probably changed some function name. So, it is impossible to do the regression test
											refactoringResult.setResult(ResultType.CHANGED_API.name());
										}
									} else{
										logger.error("not Compiled!");
										refactoringResult.setResult(ResultType.COMPILATION_ERROR.name());
									}
								} else{
									refactoringResult.setResult(ResultType.FAIL.name());
								}
//								refactoringResult.setMessage(Util.getRefactoringStatusMessage(refactoringStatus));
							} catch (Exception e) {
								refactoringResult.setResult(ResultType.EXCEPTION.name());
								refactoringResult.setMessage(e.getMessage());
							}
						}
				}//end if
			}
			resultsWriter.write(refactoringData.getResultsAsString());
			programFolderFile = new File(workspacePath + projectName + srcFolderPrefix + ++programNumber);
			stGen.addRefactoringExecutions(String.valueOf(programNumber));
			refactoringData = stGen.getExecutionDataOfProgram(String.valueOf(programNumber));
		}	
		resultsWriter.close();
	}

	
	//just refactoring functions, local variable, global variable; TODO constant?
	public void runRenameRefactoring(int cElementType){
		StatisticGenerator stGen = StatisticGenerator.getInstance();
		logger.info("Starting rename refactoring...");
		RefactoringExecutor rex = new RefactoringExecutor();
		int programNumber = 1;
		File programFolderFile = new File(workspacePath + projectName + srcFolderPrefix + programNumber);
		stGen.addRefactoringExecutions(String.valueOf(programNumber));
		RefactoringData refactoringData = stGen.getExecutionDataOfProgram(String.valueOf(programNumber));
		while(programFolderFile.exists()){
			for (File programFile: programFolderFile.listFiles()){
				
				if (programFile.getName().endsWith(".c") && !programFile.getName().startsWith("test_") ){
					
					
					ITranslationUnit tu = FuncoesGerais.abreArquivo(projectName + "/" + srcFolderPrefix + "/" + programNumber + "/" + programFile.getName());
					
					List<ICElement> functions = null;
					List<ICElement> globalVariables = null;
					List<ICElement> localVariables = null;
					List<RefactoringExpectedResult> refExpectResult = new ArrayList<RefactoringExpectedResult>();
					try {
						functions = tu.getChildrenOfType(ICElement.C_FUNCTION);
						globalVariables = tu.getChildrenOfType(ICElement.C_VARIABLE);
						localVariables = tu.getChildrenOfType(ICElement.C_VARIABLE_LOCAL);
					} catch (CModelException e1) {
						e1.printStackTrace();
					}
					
					for (ICElement element: functions){
						org.eclipse.cdt.core.model.IFunction function = (org.eclipse.cdt.core.model.IFunction) element;
						refExpectResult.add(new RefactoringExpectedResult(function.getElementName(), false));
					}
					
					for (ICElement element: globalVariables){
						org.eclipse.cdt.core.model.IVariable globalVariable = (org.eclipse.cdt.core.model.IVariable) element;
						refExpectResult.add(new RefactoringExpectedResult(globalVariable.getElementName(), false));
					}
					
					for (ICElement element: localVariables){
						org.eclipse.cdt.core.model.IVariableDeclaration localVariable = (org.eclipse.cdt.core.model.IVariableDeclaration) element;
						refExpectResult.add(new RefactoringExpectedResult(localVariable.getElementName(), false));
					}
					
					System.out.println("Ser�o refatorados para nomes: ");
					for (RefactoringExpectedResult nome: refExpectResult){
						System.out.println(nome.getNewName());
					}

					//choosing the C elements to be refactored according to cElementType
					List<ICElement> elementsToBeRefactored = null;
					String refactoringFolder = null;
					switch(cElementType){
						case ICElement.C_FUNCTION:{
							elementsToBeRefactored = functions;
							refactoringFolder = refactoredProgramFolder + "/rename-function/";
							break;
						}
						case ICElement.C_VARIABLE:{
							elementsToBeRefactored = globalVariables;
							refactoringFolder = refactoredProgramFolder + "/rename-globalvariable/";
							break;
						}
						case ICElement.C_VARIABLE_LOCAL:{
							elementsToBeRefactored = localVariables;
							refactoringFolder = refactoredProgramFolder + "/rename-localvariable/";
							break;
						}
					}
					
					//renaming
					for (ICElement element: elementsToBeRefactored){
						
						//adding an unused name
						refExpectResult.add(new RefactoringExpectedResult(element.getElementName() + "_REFACTORED", true));
						for (RefactoringExpectedResult exceptedResult: refExpectResult){
							
							//statistics
							RefactoringResult refactoringResult = new RefactoringResult();
							refactoringData.addRefactoringResult(refactoringResult);
							refactoringResult.setExpectedResult(exceptedResult);
							refactoringResult.setRefactoringType(RefactoringResult.RefactoringType.RENAME_FUNCTION.name());
							refactoringResult.setFileName(programFile.getName());
							refactoringResult.setOldName(element.getElementName());
							refactoringResult.setNewName(exceptedResult.getNewName());
							
							try {
								long initialTime = (new Date()).getTime();
								RefactoringStatus refactoringStatus = rex.runRenameRefactoring(srcFolderPrefix, programNumber, programFile.getName(), element.getElementName(), 
										exceptedResult.getNewName(), refactoringFolder, 1); //ie.: .../src/1/
								long finalTime = (new Date()).getTime();
								refactoringResult.setRefatoringExecutionTime(finalTime - initialTime);
								if (!refactoringStatus.hasError()){
									//trying to compile the program
									if (refactoringStatus.hasInfo() || refactoringStatus.hasWarning()){
										refactoringResult.setResult(ResultType.REFACTORING_INFO.name());										
									}
									
									String refactoredProgramFolder = workspacePath + "/" + projectName + "/" + refactoringFolder + "/" + programNumber + "/" + 
											element.getElementName() + "_" + exceptedResult.getNewName() + "/";
									logger.info("Refactored without error. Trying to compile in " + refactoredProgramFolder);
									boolean compiled = Compile.run(refactoredProgramFolder, true);
									
									//trying to execute regression test
									if (compiled){
										logger.info("Compiled!");
										
										//trying to execute regression tests
										FuncoesGerais.copyFilesToRegressionTests(programFolderFile.getAbsolutePath(), refactoredProgramFolder);
										//compile and run the regression tests
										boolean compiledAndRunnedRegressionTests = Compile.run(refactoredProgramFolder, false);
										if (compiledAndRunnedRegressionTests){
											RegressionTestsResult regressioTestsResults = FuncoesGerais.getRegressionTestsResult(refactoredProgramFolder);
											if (regressioTestsResults.isTestsPassed()){
												refactoringResult.setResult(ResultType.SUCCESS.name());
											}else {
												refactoringResult.setResult(ResultType.REGRESSION_TEST_FAIL.name());
												refactoringResult.setMessage(regressioTestsResults.getRegressionTestMessage());
											}
										} else{
											//changed some function name. So, it is impossible to do the regression test
											refactoringResult.setResult(ResultType.CHANGED_API.name());
										}
									} else{
										logger.error("not Compiled!");
										refactoringResult.setResult(ResultType.COMPILATION_ERROR.name());
									}
								} else{
									refactoringResult.setResult(ResultType.FAIL.name());
								}
								refactoringResult.setMessage(Util.getRefactoringStatusMessage(refactoringStatus));
							} catch (Exception e) {
								refactoringResult.setResult(ResultType.EXCEPTION.name());
								refactoringResult.setMessage(e.getMessage());
							}
						}
					}
				}//end if
			}
			programFolderFile = new File(workspacePath + projectName + srcFolderPrefix + ++programNumber);
			stGen.addRefactoringExecutions(String.valueOf(programNumber));
			refactoringData = stGen.getExecutionDataOfProgram(String.valueOf(programNumber));
		}	
	}
*/
}
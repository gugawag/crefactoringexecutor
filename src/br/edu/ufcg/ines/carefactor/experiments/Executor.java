package br.edu.ufcg.ines.carefactor.experiments;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringData;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.RefactoringType;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.ResultType;
import br.edu.ufcg.ines.carefactor.experiments.statistics.StatisticGenerator;
import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult;
import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult.NewNameType;
import br.edu.ufcg.ines.carefactor.refactorings.RefactoringExecutor;
import br.edu.ufcg.ines.carefactor.util.CommandExecutionResult;
import br.edu.ufcg.ines.carefactor.util.Compile;
import br.edu.ufcg.ines.carefactor.util.FuncoesGerais;
import br.edu.ufcg.ines.carefactor.util.RegressionTestsResult;

public class Executor {

	private static Logger logger = Logger.getLogger(Executor.class);
	
	public static String workspacePath = "/Users/gugawag/Documents/mestrado/runtime-EclipseApplication/";
	public static String projectName = "ACExperiments";
	public static String srcFolderPrefix = "/src/experiment1/";
	public static String refactoredProgramFolder = srcFolderPrefix + "/carefactor/";
	public static String errorFolder = "errors";
	public static String infoFolder = "info";
	
	
	//just refactoring functions, local variable, global variable, function parameters; TODO constant?
	public void runRenameRefactoring(int cElementType, String oldName, String newName){
		StatisticGenerator stGen = StatisticGenerator.getInstance();
		logger.info("Starting rename local variable refactoring...");
		RefactoringExecutor rex = new RefactoringExecutor();
		File programsFolderFile = new File(workspacePath + projectName + srcFolderPrefix);
		File compilationErrorFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_compilationError.properties");
		File successFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_success.properties");
		File regressionTestFailFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_regressionTestFail.properties");
		File regressionTestFailBySignalFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_regressionTestFailBySignal.properties");
		File changedApiFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_changedApi.properties");
		File failFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_fail.properties");
		File exceptionFile = new File(workspacePath + projectName + refactoredProgramFolder + "/carefactorResults_exception.properties");
		
		PrintWriter compilationErrorWriter = null;
		PrintWriter successWriter = null;
		PrintWriter regressionTestFailWriter = null;
		PrintWriter regressionTestFailBySignalWriter = null;
		PrintWriter changedApiWriter = null;
		PrintWriter failWriter = null;
		PrintWriter exceptionWriter = null;
		
		File refactoredProgramFolderFile = new File(workspacePath + projectName + refactoredProgramFolder);
		if (!refactoredProgramFolderFile.exists()){
			refactoredProgramFolderFile.mkdir();
		}
		try {
			if (!compilationErrorFile.exists()){
				try {
					compilationErrorFile.createNewFile();
					successFile.createNewFile();
					regressionTestFailFile.createNewFile();
					regressionTestFailBySignalFile.createNewFile();
					changedApiFile.createNewFile();
					failFile.createNewFile();
					exceptionFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
			}
			compilationErrorWriter = new PrintWriter(compilationErrorFile);
			successWriter = new PrintWriter(successFile);
			regressionTestFailWriter = new PrintWriter(regressionTestFailFile);
			regressionTestFailBySignalWriter = new PrintWriter(regressionTestFailBySignalFile);
			changedApiWriter = new PrintWriter(changedApiFile);
			failWriter = new PrintWriter(failFile);
			exceptionWriter = new PrintWriter(exceptionFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		List<RefactoringExpectedResult> refExpectResult = new ArrayList<RefactoringExpectedResult>();
		refExpectResult.add(new RefactoringExpectedResult(newName, NewNameType.NEW_NAME, true));

		//choosing the C elements to be refactored according to cElementType
		String refactoringFolder = null;
		RefactoringType refactoringType = null;
		switch(cElementType){
			case ICElement.C_FUNCTION:{
				refactoringFolder = refactoredProgramFolder + "/rename-function/";
				refactoringType = RefactoringType.RENAME_FUNCTION;
				refactoringNewNameValues(oldName, refExpectResult);
				break;
			}
			case ICElement.C_VARIABLE:{
				refactoringFolder = refactoredProgramFolder + "/rename-globalvariable/";
				refactoringType = RefactoringType.RENAME_GLOBAL_VARIABLE;
				refactoringNewNameValues(oldName, refExpectResult);
				break;
			}
			case ICElement.C_MACRO:{ //DEFINE
				refactoringFolder = refactoredProgramFolder + "/rename-define/";
				refactoringType = RefactoringType.RENAME_DEFINE;
				
				refactoringNewNameValues(oldName, refExpectResult);

				break;
			}
			case ICElement.C_VARIABLE_LOCAL:{ //param also. 
				refactoringFolder = refactoredProgramFolder + "/rename-local-var/";
				refactoringType = RefactoringType.RENAME_LOCAL_VARIABLE;
				
				refactoringNewNameValues(oldName, refExpectResult);

				break;
			}
		}
		File refactoringFolderFile = new File(refactoringFolder);
		if(!refactoringFolderFile.exists()){
			refactoringFolderFile.mkdir();
		}
		
		Random random = new Random();
		for (File programFolder: programsFolderFile.listFiles()){
			if (programFolder.getName().startsWith(".") || programFolder.getName().contains("cdolly.properties") || programFolder.getName().contains("carefactor")){
				continue;
			}
			
			stGen.addRefactoringExecutions(String.valueOf(programFolder.getName()));
			RefactoringData refactoringData = stGen.getExecutionDataOfProgram(programFolder.getName());
			int quantRefactorings = 1;
			
			//choosing just one refactoring (new name) to be applied
			
			int indxNewName = random.nextInt(refExpectResult.size());
			RefactoringExpectedResult expectedResult = refExpectResult.get(indxNewName);
			logger.info("Refactoring program [" + programFolder.getName() + "] to new name: " + expectedResult.getNewName());

			long initialTime = new Date().getTime();
			ResultType rType = null;
			for (File programFile: programFolder.listFiles()){
				
				if (programFile.getName().endsWith(".c") && !programFile.getName().startsWith("test_") ){
					
							//statistics
							RefactoringResult refactoringResult = new RefactoringResult();
							refactoringData.addRefactoringResult(refactoringResult);
							refactoringResult.setExpectedResult(expectedResult);
							refactoringResult.setRefactoringType(refactoringType.name());
							refactoringResult.setFileName(programFile.getName());
							refactoringResult.setOldName(oldName);
							refactoringResult.setNewName(expectedResult.getNewName());
							refactoringResult.setRefactoringNumber(quantRefactorings);
							
							try {
								long refInitialTime = (new Date()).getTime();
								//TODO apenas a linha abaixo est� espec�fica. Alter�-la para deixar gen�rica para qualquer tipo de rename
								File fileToBeRefactoredInfo = new File(Executor.workspacePath + Executor.projectName + refactoringFolder + "/" + Integer.parseInt(programFolder.getName()) + "/"+ Executor.infoFolder + "/" + oldName + "_" + quantRefactorings + "/" +     
										"/" +  programFile.getName());
								File fileToBeRefactoredError = new File(Executor.workspacePath + Executor.projectName + refactoringFolder + "/" + Integer.parseInt(programFolder.getName()) + "/"+ Executor.errorFolder + "/" + oldName + "_" + quantRefactorings + "/" +     
										"/" +  programFile.getName());

								RefactoringStatus refactoringStatus = rex.runRenameRefactoring(srcFolderPrefix, Integer.parseInt(programFolder.getName()), programFile.getName(), oldName, 
										expectedResult.getNewName(), refactoringFolder, quantRefactorings); //ie.: .../src/1/
								long finalTime = (new Date()).getTime();
								refactoringResult.setRefatoringExecutionTime(finalTime - refInitialTime);
								rType = analyseRefactoring(oldName, refactoringFolder,
										programFolder, quantRefactorings,
										refactoringResult, refactoringStatus, fileToBeRefactoredInfo, fileToBeRefactoredError);
							} catch (Exception e) {
								refactoringResult.setResult(ResultType.EXCEPTION.name());
								refactoringResult.setMessage(e.getMessage());
							}
				}//end if
			}
			long finalTime = new Date().getTime();
			logger.info("Refactoring total time (ms): " + (finalTime-initialTime));
			refactoringData.setTotalExecutionTime(finalTime-initialTime);
			switch (rType) {
			case SUCCESS:
				successWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;
			case COMPILATION_ERROR:
				compilationErrorWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;
			case REGRESSION_TEST_FAIL:
				regressionTestFailWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;
			case REGRESSION_TEST_FAIL_BY_SIGNAL:
				regressionTestFailBySignalWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;
			case CHANGED_API:
				changedApiWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;
			case FAIL:
				failWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;
			case EXCEPTION:
				exceptionWriter.write(programFolder.getName() + "=" + refactoringData.getResultsAsString() + "\n");	
				break;				
				
			default:
				logger.info("n�o caiu em nenhuma das op��es do algoritmo!!");
				break;
			}
			
			quantRefactorings++;
		}	
		compilationErrorWriter.close();
		successWriter.close();
		regressionTestFailWriter.close();
		regressionTestFailBySignalWriter.close();
		changedApiWriter.close();
		failWriter.close();
		exceptionWriter.close();
	}


	private void refactoringNewNameValues(String oldName,
			List<RefactoringExpectedResult> refExpectResult) {
		//Adding already pic name or invalide name, as function name, param name, keywords etc
		//function name
		refExpectResult.add(new RefactoringExpectedResult("Function_0", NewNameType.EXISTING_FUNCTION_NAME, false));
		
		//keyword
		refExpectResult.add(new RefactoringExpectedResult("int",  NewNameType.KEYWORD, false));
		
		//invalid name
		refExpectResult.add(new RefactoringExpectedResult("a v", NewNameType.SPACED_NAME,  false));
		
		//name with pointer
		refExpectResult.add(new RefactoringExpectedResult("*" + oldName,  NewNameType.POINTER_NAME, false));
		
		//name with pointer address: &
		refExpectResult.add(new RefactoringExpectedResult("&" + oldName,  NewNameType.POINTER_ADRESS_NAME, false));
		
		//global var name
		refExpectResult.add(new RefactoringExpectedResult("GlobalVar_0",  NewNameType.EXISTING_GLOBAL_VAR_NAME, false));
		
		//literal value
		refExpectResult.add(new RefactoringExpectedResult("0",  NewNameType.LITERAL, false));
		
		refExpectResult.add(new RefactoringExpectedResult("Id_0", NewNameType.EXISTING_DEFINE_NAME, false));
	}


	public static ResultType analyseRefactoring(String oldName, String refactoringFolder,
			File programFolder, int quantRefactorings,
			RefactoringResult refactoringResult,
			RefactoringStatus refactoringStatus, File fileToBeRefactoredInfo, File fileToBeRefactoredError) {
		
//		if ("null".equals(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.ERROR).trim())){// && !refactoringStatus.hasFatalError()){
//TODO usada linha abaixo quando est� executando de verdade, ou seja, CARefactor num �nico passo
		if (!refactoringStatus.hasError() && !refactoringStatus.hasFatalError()){
			logger.info("NAO TEM ERRO!");
//			if (!"null".equals(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.INFO).trim())){
				
				//TODO usada linha abaixo quando est� executando de verdade, ou seja, CARefactor num �nico passo
			if (refactoringStatus.hasInfo()){
				refactoringResult.setResult(ResultType.REFACTORING_INFO.name());		
				refactoringResult.setInfoMessage(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.INFO));
				refactoringResult.setWarningMessage(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.WARNING));
			}
			
			String refactoredProgramFolder = workspacePath + "/" + projectName + "/" + refactoringFolder + "/" + programFolder.getName() + "/" + 
					oldName + "_" + quantRefactorings + "/";
			CommandExecutionResult compiled = Compile.run(refactoredProgramFolder, true);
			
			//trying to execute regression test
			if (compiled.isCompiled()){
				logger.info("Compiled!");
				
				String regressionTestsFolder = workspacePath + "/" + "/catester_output/" + srcFolderPrefix + programFolder.getName() + "/tests";  
				//trying to execute regression tests
				FuncoesGerais.copyFilesToRegressionTests(regressionTestsFolder, refactoredProgramFolder);
				//compile and run the regression tests
				CommandExecutionResult compileExecutionResult = Compile.run(refactoredProgramFolder, false);
				logger.info("aqui");
				if (compileExecutionResult.isCompiled()){
					//executing regression tests
					FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/CAutomaticRefactor/resources/execute.sh " + refactoredProgramFolder, null);
					
					CommandExecutionResult regressionExecutionResult = FuncoesGerais.executaComando(refactoredProgramFolder + 
							"execute.sh a.out " + refactoredProgramFolder, null);
					if (regressionExecutionResult.getExitVal()==0){
						RegressionTestsResult regressioTestsResult = Compile.compileAndRunRegressionTests(refactoredProgramFolder);
						if (regressioTestsResult.isTestsPassed()){
							refactoringResult.setResult(ResultType.SUCCESS.name());
							return ResultType.SUCCESS;
						}else {
							refactoringResult.setResult(ResultType.REGRESSION_TEST_FAIL.name());
							refactoringResult.setMessage(regressioTestsResult.getRegressionTestMessage());
							refactoringResult.setCommandExecutionResult(compileExecutionResult);
							return ResultType.REGRESSION_TEST_FAIL;
						}
					} else{
						refactoringResult.setResult(ResultType.REGRESSION_TEST_FAIL_BY_SIGNAL.name());
						refactoringResult.setCommandExecutionResult(regressionExecutionResult);
						return ResultType.REGRESSION_TEST_FAIL_BY_SIGNAL;
					}
				} else{
					//probably changed some function name
					refactoringResult.setResult(ResultType.CHANGED_API.name());
					refactoringResult.setCommandExecutionResult(compileExecutionResult);
					return ResultType.CHANGED_API;
				}
			} else{
				refactoringResult.setResult(ResultType.COMPILATION_ERROR.name());
				refactoringResult.setCommandExecutionResult(compiled);
				return ResultType.COMPILATION_ERROR;
			}
		} else{
			refactoringResult.setResult(ResultType.FAIL.name());
			refactoringResult.setErrorMessage(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.ERROR));
			refactoringResult.setFatalMessage(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.FATAL));
			refactoringResult.setInfoMessage(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.INFO));
			refactoringResult.setWarningMessage(refactoringStatus.getMessageMatchingSeverity(RefactoringStatus.WARNING));
			try {
				
				if (fileToBeRefactoredInfo.exists()){
					Scanner scanner = new Scanner(new FileInputStream(fileToBeRefactoredInfo));
					StringBuffer strBuffer = new StringBuffer();
					while(scanner.hasNextLine()){
						strBuffer.append(scanner.nextLine());
					}
					refactoringResult.setMessage(strBuffer.toString());
					scanner.close();
				}
				if (fileToBeRefactoredError.exists()){
					Scanner scanner = new Scanner(new FileInputStream(fileToBeRefactoredError));
					StringBuffer strBuffer = new StringBuffer();
					while(scanner.hasNextLine()){
						strBuffer.append(scanner.nextLine());
					}
					refactoringResult.setMessage(strBuffer.toString());
					scanner.close();
				}

				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return ResultType.FAIL;
		}
	}

	
/*	//just refactoring functions, local variable, global variable; TODO constant?
	public void runRenameRefactoring(int cElementType){
		StatisticGenerator stGen = StatisticGenerator.getInstance();
		logger.info("Starting rename refactoring...");
		RefactoringExecutor rex = new RefactoringExecutor();
		int programNumber = 1;
		File programFolderFile = new File(workspacePath + projectName + srcFolderPrefix + programNumber);
		stGen.addRefactoringExecutions(String.valueOf(programNumber));
		RefactoringData refactoringData = stGen.getExecutionDataOfProgram(String.valueOf(programNumber));
		while(programFolderFile.exists()){
			for (File programFile: programFolderFile.listFiles()){
				
				if (programFile.getName().endsWith(".c") && !programFile.getName().startsWith("test_") ){
					
					
					ITranslationUnit tu = FuncoesGerais.abreArquivo(projectName + "/" + srcFolderPrefix + "/" + programNumber + "/" + programFile.getName());
					
					List<ICElement> functions = null;
					List<ICElement> globalVariables = null;
					List<ICElement> localVariables = null;
					List<RefactoringExpectedResult> refExpectResult = new ArrayList<RefactoringExpectedResult>();
					try {
						functions = tu.getChildrenOfType(ICElement.C_FUNCTION);
						globalVariables = tu.getChildrenOfType(ICElement.C_VARIABLE);
						localVariables = tu.getChildrenOfType(ICElement.C_VARIABLE_LOCAL);
					} catch (CModelException e1) {
						e1.printStackTrace();
					}
					
					for (ICElement element: functions){
						org.eclipse.cdt.core.model.IFunction function = (org.eclipse.cdt.core.model.IFunction) element;
						refExpectResult.add(new RefactoringExpectedResult(function.getElementName(), false));
					}
					
					for (ICElement element: globalVariables){
						org.eclipse.cdt.core.model.IVariable globalVariable = (org.eclipse.cdt.core.model.IVariable) element;
						refExpectResult.add(new RefactoringExpectedResult(globalVariable.getElementName(), false));
					}
					
					for (ICElement element: localVariables){
						org.eclipse.cdt.core.model.IVariableDeclaration localVariable = (org.eclipse.cdt.core.model.IVariableDeclaration) element;
						refExpectResult.add(new RefactoringExpectedResult(localVariable.getElementName(), false));
					}
					
					System.out.println("Ser�o refatorados para nomes: ");
					for (RefactoringExpectedResult nome: refExpectResult){
						System.out.println(nome.getNewName());
					}

					//choosing the C elements to be refactored according to cElementType
					List<ICElement> elementsToBeRefactored = null;
					String refactoringFolder = null;
					switch(cElementType){
						case ICElement.C_FUNCTION:{
							elementsToBeRefactored = functions;
							refactoringFolder = refactoredProgramFolder + "/rename-function/";
							break;
						}
						case ICElement.C_VARIABLE:{
							elementsToBeRefactored = globalVariables;
							refactoringFolder = refactoredProgramFolder + "/rename-globalvariable/";
							break;
						}
						case ICElement.C_VARIABLE_LOCAL:{
							elementsToBeRefactored = localVariables;
							refactoringFolder = refactoredProgramFolder + "/rename-localvariable/";
							break;
						}
					}
					
					//renaming
					for (ICElement element: elementsToBeRefactored){
						
						//adding an unused name
						refExpectResult.add(new RefactoringExpectedResult(element.getElementName() + "_REFACTORED", true));
						for (RefactoringExpectedResult exceptedResult: refExpectResult){
							
							//statistics
							RefactoringResult refactoringResult = new RefactoringResult();
							refactoringData.addRefactoringResult(refactoringResult);
							refactoringResult.setExpectedResult(exceptedResult);
							refactoringResult.setRefactoringType(RefactoringResult.RefactoringType.RENAME_FUNCTION.name());
							refactoringResult.setFileName(programFile.getName());
							refactoringResult.setOldName(element.getElementName());
							refactoringResult.setNewName(exceptedResult.getNewName());
							
							try {
								long initialTime = (new Date()).getTime();
								RefactoringStatus refactoringStatus = rex.runRenameRefactoring(srcFolderPrefix, programNumber, programFile.getName(), element.getElementName(), 
										exceptedResult.getNewName(), refactoringFolder, 1); //ie.: .../src/1/
								long finalTime = (new Date()).getTime();
								refactoringResult.setRefatoringExecutionTime(finalTime - initialTime);
								if (!refactoringStatus.hasError()){
									//trying to compile the program
									if (refactoringStatus.hasInfo() || refactoringStatus.hasWarning()){
										refactoringResult.setResult(ResultType.REFACTORING_INFO.name());										
									}
									
									String refactoredProgramFolder = workspacePath + "/" + projectName + "/" + refactoringFolder + "/" + programNumber + "/" + 
											element.getElementName() + "_" + exceptedResult.getNewName() + "/";
									logger.info("Refactored without error. Trying to compile in " + refactoredProgramFolder);
									boolean compiled = Compile.run(refactoredProgramFolder, true);
									
									//trying to execute regression test
									if (compiled){
										logger.info("Compiled!");
										
										//trying to execute regression tests
										FuncoesGerais.copyFilesToRegressionTests(programFolderFile.getAbsolutePath(), refactoredProgramFolder);
										//compile and run the regression tests
										boolean compiledAndRunnedRegressionTests = Compile.run(refactoredProgramFolder, false);
										if (compiledAndRunnedRegressionTests){
											RegressionTestsResult regressioTestsResults = FuncoesGerais.getRegressionTestsResult(refactoredProgramFolder);
											if (regressioTestsResults.isTestsPassed()){
												refactoringResult.setResult(ResultType.SUCCESS.name());
											}else {
												refactoringResult.setResult(ResultType.REGRESSION_TEST_FAIL.name());
												refactoringResult.setMessage(regressioTestsResults.getRegressionTestMessage());
											}
										} else{
											//changed some function name. So, it is impossible to do the regression test
											refactoringResult.setResult(ResultType.CHANGED_API.name());
										}
									} else{
										logger.error("not Compiled!");
										refactoringResult.setResult(ResultType.COMPILATION_ERROR.name());
									}
								} else{
									refactoringResult.setResult(ResultType.FAIL.name());
								}
								refactoringResult.setMessage(Util.getRefactoringStatusMessage(refactoringStatus));
							} catch (Exception e) {
								refactoringResult.setResult(ResultType.EXCEPTION.name());
								refactoringResult.setMessage(e.getMessage());
							}
						}
					}
				}//end if
			}
			programFolderFile = new File(workspacePath + projectName + srcFolderPrefix + ++programNumber);
			stGen.addRefactoringExecutions(String.valueOf(programNumber));
			refactoringData = stGen.getExecutionDataOfProgram(String.valueOf(programNumber));
		}	
	}*/

}
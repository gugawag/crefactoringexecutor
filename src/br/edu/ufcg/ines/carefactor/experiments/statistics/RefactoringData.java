package br.edu.ufcg.ines.carefactor.experiments.statistics;

import java.util.ArrayList;
import java.util.List;

import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.ResultType;


public class RefactoringData {
	
	private List<RefactoringResult> results = new ArrayList<RefactoringResult>();

	private long totalExecutionTime = 0;
	
	private String programName;
	

	public RefactoringData(String programName) {
		super();
		this.programName = programName;
	}

	public void addRefactoringResult(RefactoringResult result){
		results.add(result);
	}

	public List<RefactoringResult> getResults() {
		return results;
	}
	
	public String getResultsAsString(){
		StringBuffer resultBuffer = new StringBuffer();
		for (int i=0; i< results.size(); i++){
			RefactoringResult result = results.get(i);
			String resultMessage = result.getMessage()==null?"":result.getMessage();
			resultBuffer.append(result.isSucessRefactoring());
			resultBuffer.append(";");
			resultBuffer.append(result.getFileName());
			resultBuffer.append(";");
			resultBuffer.append(result.getResult());
			resultBuffer.append("; MESSAGE [" + resultMessage + "]");  
			resultBuffer.append("; MESSAGE_ERROR [" + result.getErrorMessage() + "]");
			resultBuffer.append("; MESSAGE_INFO [" + result.getInfoMessage() + "]");
			resultBuffer.append("; MESSAGE_WARNING [" + result.getWarningMessage() + "]");
			resultBuffer.append(";" + (result.getCommandExecutionResult()==null?"":result.getCommandExecutionResult().toString()) + ";");
			resultBuffer.append(result.getRefactoringType());
			resultBuffer.append(";");
			resultBuffer.append(result.getExpectedResult().getNewNameType().name());
			resultBuffer.append(";");
			resultBuffer.append(result.getOldName());
			resultBuffer.append(";");
			resultBuffer.append(result.getNewName());
			resultBuffer.append(";");
			resultBuffer.append(result.getRefactoringNumber());
			resultBuffer.append(";");
			resultBuffer.append(result.getRefatoringExecutionTime() + ",");
			totalExecutionTime += result.getRefatoringExecutionTime();
		}
		//Removing the last comma
		if (resultBuffer.length() > 0){
			return resultBuffer.substring(0, resultBuffer.length()-1);
		}
		resultBuffer.append("--" + getTotalExecutionTimeInSeconds());
		return resultBuffer.toString();
	}

	public long getTotalExecutionTime() {
		return totalExecutionTime;
	}

	public void setTotalExecutionTime(long totalExecutionTime) {
		this.totalExecutionTime = totalExecutionTime;
	}
	
	public long getTotalExecutionTimeInSeconds(){
		return totalExecutionTime/1000;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}
	
	
		
}

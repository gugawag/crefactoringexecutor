package br.edu.ufcg.ines.carefactor.experiments.statistics;

import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult;
import br.edu.ufcg.ines.carefactor.util.CommandExecutionResult;


public class RefactoringResult {

	public RefactoringExpectedResult getExpectedResult() {
		return expectedResult;
	}

	public enum RefactoringType{
		RENAME_FILE, RENAME_FUNCTION, RENAME_LOCAL_VARIABLE, RENAME_GLOBAL_VARIABLE, RENAME_PARAM, EXTRACT_LOCAL_VARIABLE, EXTRACT_CONSTANT, EXTRACT_FUNCTION, TOGGLE_FUNCTION, RENAME_DEFINE;
	}
	
	//Results: 0=fail; 1=success; ...
	//EXCEPTION: exception running the refactoring
	public enum ResultType{
		FAIL, SUCCESS, REFACTORING_INFO, COMPILATION_ERROR, REGRESSION_ERROR, EXCEPTION, REGRESSION_TEST_FAIL, CHANGED_API, REGRESSION_TEST_FAIL_BY_SIGNAL;
	}
	
	
	//represents a message if the refactoring fail, for example
	private String message;
	
	private String infoMessage;
	private String errorMessage;
	private String warningMessage;
	private String fatalMessage;
	

	private String result;
	
	private String fileName;
	
	private String oldName;
	private String newName;
	
	private String refactoringType;
	
	private long refatoringExecutionTime;

	private RefactoringExpectedResult expectedResult;

	private int refactoringNumber;

	private CommandExecutionResult commandExecutionResult; 
	
	public long getRefatoringExecutionTime() {
		return refatoringExecutionTime;
	}

	public void setRefatoringExecutionTime(long refatoringExecutionTime) {
		this.refatoringExecutionTime = refatoringExecutionTime;
	}

	public RefactoringResult() {
		this(null,null, null, null, null, null);
	}
	
	public RefactoringResult(ResultType type) {
		this(type,null, null, null, null, null);
	}

	public RefactoringResult(ResultType type, String RefactoringType, String message, String fileName, String oldName, String newName) {
		super();
		if (type != null){
			this.result = type.name();
		}
		this.message = message;
		this.fileName = fileName;
		this.oldName = oldName;
		this.newName = newName;
		this.refactoringType = RefactoringType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOldName() {
		return oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public String getRefactoringType() {
		return refactoringType;
	}

	public void setRefactoringType(String refactoringType) {
		this.refactoringType = refactoringType;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setExpectedResult(RefactoringExpectedResult exceptedResult) {
		this.expectedResult = exceptedResult;
	}
	
	//Result different from expected
	public boolean isSucessRefactoring(){
		return expectedResult.isSucessRefactoring();
	}

	public void setRefactoringNumber(int quantRefactorings) {
		this.refactoringNumber = quantRefactorings;
	}
	
	public String getInfoMessage() {
		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getRefactoringNumber() {
		return refactoringNumber;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public String getFatalMessage() {
		return fatalMessage;
	}

	public void setFatalMessage(String fatalMessage) {
		this.fatalMessage = fatalMessage;
	}

	public void setCommandExecutionResult(
			CommandExecutionResult regressionExecutionResult) {
		this.commandExecutionResult = regressionExecutionResult;
	}

	public CommandExecutionResult getCommandExecutionResult() {
		return commandExecutionResult;
	}
	
	
	
	
}

package br.edu.ufcg.ines.carefactor.app;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

public class App {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
//		String regex = "x(?=\\s|,|;|\\))";
////		regex = "x[;\\s,)]";
//		String texto = "int x = 1; int y = x; int y = f(x, w); int y = f(w, x);";
//		String novoTexto = texto.replaceAll(regex, "nova");
//		System.out.println(texto);
//		System.out.println(novoTexto);
		
	}
	
	public static void configuraLog4j() throws IOException{
		RollingFileAppender rfappender = new RollingFileAppender(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %c{1} [%p] %m%n"), "/Users/gugawag/Documents/mestrado/workspace/CAutomaticRefactor/logs/carefactor.log");
		rfappender.setThreshold(Level.INFO);
		BasicConfigurator.configure(rfappender);

		//desligar quando estiver fazendo experimentos
//		BasicConfigurator.configure(new ConsoleAppender(new PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %c{1} [%p] %m%n")));
	}

}

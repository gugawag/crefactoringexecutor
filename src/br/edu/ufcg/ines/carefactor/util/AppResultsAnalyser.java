package br.edu.ufcg.ines.carefactor.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class AppResultsAnalyser {

	private static String refactoringsFolder = "/Users/gugawag/Documents/mestrado/experimentos/experiment1-rename_define/run 2 but 3 Stmt 25-01-2016/carefactor/";
	private static String refactoringType="RENAME_DEFINE;";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		printCompilationErrorResults();
//		printRegressionTestFailResults();
	}

	private static void printCompilationErrorResults() throws IOException,
			FileNotFoundException {
		String[]errorMessages = new String[]{"cannot combine with previous", "expected ')'", "use of undeclared identifier"};
		File folderResultsFile = new File(refactoringsFolder);
		Properties propertiesCompilationError = new Properties();
		propertiesCompilationError.load(new FileInputStream(folderResultsFile.getAbsolutePath()+"/carefactorResults_compilationError.properties"));
		
		Map<String, List<String>> mapMessagesExpectedRefactoring = new HashMap<String, List<String>>();
		
		//example: false;f18205.c;COMPILATION_ERROR; MESSAGE []; MESSAGE_ERROR [null]; MESSAGE_INFO [null]; MESSAGE_WARNING [null];COMMAND_RETURN [null|f18205.c:5:20: error: cannot combine with previous 'int' declaration specifier|void main_0 (
		int quantPrograms = 0;
		for (Object program: propertiesCompilationError.keySet()){
			try{
//				boolean expectedRefactoring = Boolean.parseBoolean(((String)propertiesCompilationError.get(program)).split(";")[0]);
				String inputType = (((String)propertiesCompilationError.get(program)).split(refactoringType)[1]).split(";")[0];
				String rightSide = ((String)propertiesCompilationError.get(program)).split("error:")[1];
				String errorMessage = removeSpecificValues(rightSide.split("\\|")[0]);
				boolean hasCorrespondentMessage = false;
						for(String message: errorMessages){
//							String key = expectedRefactoring + ";" + message + ";" + inputType;
							message = removeSpecificValues(message);
							String key = message + ";" + inputType;
							if (errorMessage.contains(message)){
								List<String> programs = mapMessagesExpectedRefactoring.get(key);
								if (programs == null){
									programs = new ArrayList<String>();
								}
								programs.add((String)program);
								mapMessagesExpectedRefactoring.put(key, programs);
								hasCorrespondentMessage = true;
								break;
							}							

						}
						if (!hasCorrespondentMessage){
//							String key = expectedRefactoring + ";" + errorMessage + ";" + inputType;
							String key = errorMessage + ";" + inputType;
							System.out.println("KEY: " + key);
							List<String> programs = mapMessagesExpectedRefactoring.get(key);
							if (programs == null){
								programs = new ArrayList<String>();
							}
							programs.add((String)program);
	
							mapMessagesExpectedRefactoring.put(key, programs);
						}


			}catch(RuntimeException e){
			}
			quantPrograms++;
		}
		PrintWriter writer = new PrintWriter(folderResultsFile.getAbsolutePath() + "/compilationErrors.result");
		writer.write("#programs compile failed: " + quantPrograms + "\n");
		writer.write("#bugs: " + (mapMessagesExpectedRefactoring.size()) + "\n");
		for (String key: mapMessagesExpectedRefactoring.keySet()){
			List<String> programs = mapMessagesExpectedRefactoring.get(key);
			writer.write("\n" + key + ": " + programs.size());
			System.out.println("\n" + key + ": " + programs.size());
			for (String program: programs){
				writer.write("," + program);
				System.out.println("," + program);
			}
		}
		writer.close();
	}
	
	public static String removeSpecificValues(String string) {
		return string.replaceAll("'[a-zA-Z_0-9]*'", "'x'");
	}

	private static void printRegressionTestFailResults() throws IOException,
		FileNotFoundException {
		File folderResultsFile = new File(refactoringsFolder);
		Properties propertiesRegressionFail = new Properties();
		propertiesRegressionFail.load(new FileInputStream(folderResultsFile.getAbsolutePath()+"/carefactorResults_RegressionTestFail.properties"));
		Properties propertiesRegressionFailBySignal = new Properties();
		propertiesRegressionFailBySignal.load(new FileInputStream(folderResultsFile.getAbsolutePath()+"/carefactorResults_RegressionTestFailBySignal.properties"));
		
		Map<String, List<String>> mapMessagesExpectedRefactoring = new HashMap<String, List<String>>();
		Map<String, List<String>> mapProgramsRegressionBySignal = new HashMap<String, List<String>>();
		
		int quantPrograms = 0;
		for (Object program: propertiesRegressionFail.keySet()){
			try{
				boolean expectedRefactoring = Boolean.parseBoolean(((String)propertiesRegressionFail.get(program)).split(";")[0]);
				
				String inputType = (((String)propertiesRegressionFail.get(program)).split(refactoringType)[1]).split(";")[0];
				String key = inputType;
				List<String> programs = mapMessagesExpectedRefactoring.get(key);
				if (programs == null){
					programs = new ArrayList<String>();
				}
				programs.add((String)program);
				mapMessagesExpectedRefactoring.put(key, programs);
		
			}catch(RuntimeException e){
				e.printStackTrace();
			}
			quantPrograms++;
		}

		
	
		PrintWriter writer = new PrintWriter(folderResultsFile.getAbsolutePath() + "/regressionTestFail.result");
		writer.write("#programs regression test failed: " + quantPrograms + "\n");
		System.out.println("#programs regression test failed: " + quantPrograms + "\n");
		writer.write("#bugs: " + (mapMessagesExpectedRefactoring.size()) + "\n");
		System.out.println("#bugs: " + (mapMessagesExpectedRefactoring.size()) + "\n");
		for (String key: mapMessagesExpectedRefactoring.keySet()){
			List<String> programs = mapMessagesExpectedRefactoring.get(key);
			writer.write("\n" + key + ": " + programs.size());
			for (String program: programs){
				writer.write("," + program);
			}
		}
		writer.close();
		
		int quantProgramsBySignal = 0;
		
		for (Object program: propertiesRegressionFailBySignal.keySet()){
			String exitValWithBrackets = ((String)propertiesRegressionFailBySignal.get(program)).split("COMMAND_EXIT_VAL")[1].split(";")[0];
			String exitValSignal = exitValWithBrackets.substring(2, exitValWithBrackets.length()-1);
			List<String> programs = mapProgramsRegressionBySignal.get(exitValSignal);
			if (programs == null){
				programs = new ArrayList<String>();
			}
			programs.add((String)program);
			mapProgramsRegressionBySignal.put(exitValSignal, programs);
			quantProgramsBySignal++;
			
		}

		PrintWriter writerBySignal = new PrintWriter(folderResultsFile.getAbsolutePath() + "/regressionTestFailBySignal.result");
		writerBySignal.write("#programs regression test failed by signal: " + quantProgramsBySignal + "\n");
		System.out.println("#programs regression test failed by signal: " + quantProgramsBySignal + "\n");
		writerBySignal.write("#bugs: " + (mapProgramsRegressionBySignal.size()) + "\n");
		System.out.println("#bugs: " + (mapProgramsRegressionBySignal.size()) + "\n");
		for (String key: mapProgramsRegressionBySignal.keySet()){
			List<String> programs = mapProgramsRegressionBySignal.get(key);
			writerBySignal.write("\n" + key + ": " + programs.size());
			for (String program: programs){
				writerBySignal.write("," + program);
			}
		}
		writerBySignal.close();
		
	}
	
	
}

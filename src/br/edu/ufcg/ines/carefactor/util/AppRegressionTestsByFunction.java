package br.edu.ufcg.ines.carefactor.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import br.edu.ufcg.ines.carefactor.experiments.Executor;
import br.edu.ufcg.ines.catester.model.testpatterns.RandoopUnitPatterns;

public class AppRegressionTestsByFunction {
	
	public static void main(String[] args) throws Exception{
		File regressionFile = new File(Executor.workspacePath + Executor.projectName + "/src/test_regressao.c");
		Scanner scanner = new Scanner(regressionFile);
		
		long initialTime = new Date().getTime();
		Map<String, String> functionCallsMap = new HashMap<String, String>();
		StringBuilder regressionTestSource = new StringBuilder();
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			if (line.contains("int main(void)")){
				regressionTestSource.append(line.replace("main", "_main")+ "\n");	
			} else{
				regressionTestSource.append(line+ "\n");
			}
			if (line.contains("void test_")){
				String finalLine = line.replace("void ", "").replace("(void){", "();");
				functionCallsMap.put(finalLine, line.replace("{", ";"));
			}
		}
		
		//renaming main function in test_regressao.c file. 
		FileOutputStream fos = new FileOutputStream(regressionFile);
        fos.write(regressionTestSource.toString().getBytes());
		
		//scanning all the functions, generating specific main files
		RandoopUnitPatterns unitPatterns = new RandoopUnitPatterns();
		StringBuilder mainBuilder = new StringBuilder();
		for (String functionCall: functionCallsMap.keySet()){
			mainBuilder.append(functionCallsMap.get(functionCall) + "\n");
			mainBuilder.append(unitPatterns.getPadraoMain() + "\n");
			mainBuilder.append(functionCall + "\n}");
		}
		
		long finalTime = new Date().getTime();
		System.out.println(finalTime - initialTime);
		
		
//		ITranslationUnit tu = FuncoesGerais.abreArquivo("ACExperiments"
//				+ "/" + "src/" + "test_regressao.c");
//		
//		//Functions
//		List<ICElement> functions = tu.getChildrenOfType(ICElement.C_FUNCTION);
//		for(ICElement function: functions){
//			System.out.println(function.getElementName());
//		}
	}
	
	public static void replaceSelected(String replaceWith, String type) {
	    try {
	        // input the file content to the String "input"
	        BufferedReader file = new BufferedReader(new FileReader("notes.txt"));
	        String line;String input = "";

	        while ((line = file.readLine()) != null) input += line + '\n';

	        System.out.println(input); // check that it's inputted right

	        // this if structure determines whether or not to replace "0" or "1"
	        if (Integer.parseInt(type) == 0)
	        {
	            input = input.replace(replaceWith + "1", replaceWith + "0"); 
	        }
	        else if (Integer.parseInt(type) == 1) 
	        {
	            input = input.replace(replaceWith + "0", replaceWith + "1");
	        } 

	        // check if the new input is right
	        System.out.println("----------------------------------"  + '\n' + input);

	        // write the new String with the replaced line OVER the same file
	        FileOutputStream File = new FileOutputStream("notes.txt");
	        File.write(input.getBytes());

	   } catch (Exception e) {
	        System.out.println("Problem reading file.");
	    }
	}

}

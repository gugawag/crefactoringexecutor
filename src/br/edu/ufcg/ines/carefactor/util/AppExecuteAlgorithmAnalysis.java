package br.edu.ufcg.ines.carefactor.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import br.edu.ufcg.ines.carefactor.app.App;
import br.edu.ufcg.ines.carefactor.experiments.Executor;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringData;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.RefactoringType;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringResult.ResultType;
import br.edu.ufcg.ines.carefactor.experiments.statistics.StatisticGenerator;
import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult;
import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult.NewNameType;
import br.edu.ufcg.ines.carefactor.refactorings.RefactoringExecutor;

/*
 * Usar apenas quando o processo sequencial de refatoramento e an�lise n�o funcionar. 
 * Essa classe faz a an�lise dos resultados ap�s j� terem sido gerados os arquivos refatorados pelo CARefactor
 */
public class AppExecuteAlgorithmAnalysis {

	private static Logger logger = Logger.getLogger(AppExecuteAlgorithmAnalysis.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception{
		App.configuraLog4j();
		
		int cElementType = ICElement.C_VARIABLE_LOCAL;
		String oldName = "aa_0";
		
		StatisticGenerator stGen = StatisticGenerator.getInstance();
		logger.info("Starting rename local variable refactoring...");
		RefactoringExecutor rex = new RefactoringExecutor();
		File programsFolderFile = new File(Executor.workspacePath + Executor.projectName + Executor.srcFolderPrefix);
		
		File compilationErrorFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_compilationError.properties");
		File successFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_success.properties");
		File regressionTestFailFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_regressionTestFail.properties");
		File regressionTestFailBySignalFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_regressionTestFailBySignal.properties");
		File changedApiFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_changedApi.properties");
		File failFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_fail.properties");
		File exceptionFile = new File(Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-param_exception.properties");
		
		PrintWriter compilationErrorWriter = null;
		PrintWriter successWriter = null;
		PrintWriter regressionTestFailWriter = null;
		PrintWriter regressionTestFailBySignalWriter = null;
		PrintWriter changedApiWriter = null;
		PrintWriter failWriter = null;
		PrintWriter exceptionWriter = null;
		
		try {
			if (!compilationErrorFile.exists()){
				try {
					compilationErrorFile.createNewFile();
					successFile.createNewFile();
					regressionTestFailFile.createNewFile();
					regressionTestFailBySignalFile.createNewFile();
					changedApiFile.createNewFile();
					failFile.createNewFile();
					exceptionFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
			}
			compilationErrorWriter = new PrintWriter(compilationErrorFile);
			successWriter = new PrintWriter(successFile);
			regressionTestFailWriter = new PrintWriter(regressionTestFailFile);
			regressionTestFailBySignalWriter = new PrintWriter(regressionTestFailBySignalFile);
			changedApiWriter = new PrintWriter(changedApiFile);
			failWriter = new PrintWriter(failFile);
			exceptionWriter = new PrintWriter(exceptionFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		//choosing the C elements to be refactored according to cElementType
		String refactoringFolder = null;
		RefactoringType refactoringType = null;
		switch(cElementType){
			case ICElement.C_FUNCTION:{
				refactoringFolder = Executor.refactoredProgramFolder + "/rename-function/";
				refactoringType = RefactoringType.RENAME_FUNCTION;
				break;
			}
			case ICElement.C_VARIABLE:{
				refactoringFolder = Executor.refactoredProgramFolder + "/rename-globalvariable/";
				refactoringType = RefactoringType.RENAME_GLOBAL_VARIABLE;
				break;
			}
			case ICElement.C_VARIABLE_LOCAL:{ //param also. 
				refactoringFolder = Executor.refactoredProgramFolder + "/rename-param/";
				refactoringType = RefactoringType.RENAME_PARAM;
				break;
			}
		}
		File cARefactorResultsFile = new File("/Users/gugawag/Documents/mestrado/experimentos/experiment1-rename_param/carefactor/carefactorResults_rename-localvariables.properties");
		Map<String, RefactoringStatusWithName> programsMap = getProgramsWithRefactoringNames(cARefactorResultsFile);
		


		for (File programFolder: programsFolderFile.listFiles()){
			if (programFolder.getName().startsWith(".") || programFolder.getName().contains("cdolly.properties") || programFolder.getName().contains("carefactor")){
				continue;
			}
			System.out.println("PROGRAM: " + programFolder.getName());
			RefactoringStatusWithName refactoringStatusWithName = programsMap.get(programFolder.getName());
			if (refactoringStatusWithName == null){
				continue;
			}
			RefactoringStatus refactoringStatus = refactoringStatusWithName.getRefactoringStatus();
			String newName = refactoringStatusWithName.getName();
			RefactoringExpectedResult expectedResult = null;
			logger.info("New Name: " + newName);
			if (newName.equals("Function_0")){
				expectedResult = new RefactoringExpectedResult("Function_0", NewNameType.EXISTING_FUNCTION_NAME, false);
			} else if (newName.equals("aa_0_REFACTORED")){
				expectedResult = new RefactoringExpectedResult(newName, NewNameType.NEW_NAME, true);
			} else if (newName.equals("int")){
				expectedResult = new RefactoringExpectedResult("int",  NewNameType.KEYWORD, false);
			} else if (newName.equals("a v")){
				expectedResult = new RefactoringExpectedResult("a v", NewNameType.SPACED_NAME,  false);
			} else if (newName.equals("*" + oldName)){
				expectedResult = new RefactoringExpectedResult("*" + oldName,  NewNameType.POINTER_NAME, false);
			} else if (newName.equals("&" + oldName)){
				expectedResult = new RefactoringExpectedResult("&" + oldName,  NewNameType.POINTER_ADRESS_NAME, false);
			} else if (newName.equals("GlobalVar_0")){
				expectedResult = new RefactoringExpectedResult("GlobalVar_0",  NewNameType.EXISTING_GLOBAL_VAR_NAME, false);
			} else if (newName.equals("0")){
				expectedResult = new RefactoringExpectedResult("0",  NewNameType.LITERAL, false);
			}
			
			stGen.addRefactoringExecutions(String.valueOf(programFolder.getName()));
			RefactoringData refactoringData = stGen.getExecutionDataOfProgram(programFolder.getName());
			int quantRefactorings = 1;
			
			long initialTime = new Date().getTime();
			ResultType rType = null;
			for (File programFile: programFolder.listFiles()){
				
				if (programFile.getName().endsWith(".c") && !programFile.getName().startsWith("test_") ){
					
							//statistics
							RefactoringResult refactoringResult = new RefactoringResult();
							refactoringData.addRefactoringResult(refactoringResult);
							refactoringResult.setExpectedResult(expectedResult);
							refactoringResult.setRefactoringType(refactoringType.name());
							refactoringResult.setFileName(programFile.getName());
							refactoringResult.setOldName(oldName);
							refactoringResult.setNewName(expectedResult.getNewName());
							refactoringResult.setRefactoringNumber(quantRefactorings);
							
							try {
								File fileToBeRefactoredInfo = new File(Executor.workspacePath + Executor.projectName + refactoringFolder + "/" + Integer.parseInt(programFolder.getName()) + "/"+ Executor.infoFolder + "/" + oldName + "_" + 1 + "/" +     
										"/" +  programFile.getName());
								File fileToBeRefactoredError = new File(Executor.workspacePath + Executor.projectName + refactoringFolder + "/" + Integer.parseInt(programFolder.getName()) + "/"+ Executor.errorFolder + "/" + oldName + "_" + 1 + "/" +     
										"/" +  programFile.getName());

								rType = Executor.analyseRefactoring(oldName, refactoringFolder,
										programFolder, quantRefactorings,
										refactoringResult, refactoringStatus, fileToBeRefactoredInfo, fileToBeRefactoredError);
							} catch (Exception e) {
								refactoringResult.setResult(ResultType.EXCEPTION.name());
								refactoringResult.setMessage(e.getMessage());
							}
				}//end if
			}
			long finalTime = new Date().getTime();
			logger.info("Refactoring total time (ms): " + (finalTime-initialTime));
			refactoringData.setTotalExecutionTime(finalTime-initialTime);
			String resultAsString = refactoringData.getResultsAsString();
			logger.info(programFolder.getName() + "=" + resultAsString);
			switch (rType) {
			case SUCCESS:
				successWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;
			case COMPILATION_ERROR:
				compilationErrorWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;
			case REGRESSION_TEST_FAIL:
				regressionTestFailWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;
			case REGRESSION_TEST_FAIL_BY_SIGNAL:
				regressionTestFailBySignalWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;
			case CHANGED_API:
				changedApiWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;
			case FAIL:
				failWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;
			case EXCEPTION:
				exceptionWriter.write(programFolder.getName() + "=" + resultAsString + "\n");	
				break;				
				
			default:
				logger.info("n�o caiu em nenhuma das op��es do algoritmo!!");
				break;
			}
			
			quantRefactorings++;
		}	
		compilationErrorWriter.close();
		successWriter.close();
		regressionTestFailWriter.close();
		regressionTestFailBySignalWriter.close();
		changedApiWriter.close();
		failWriter.close();
		exceptionWriter.close();
	}

	private static Map<String, RefactoringStatusWithName> getProgramsWithRefactoringNames(
			File cARefactorResultsFile) throws FileNotFoundException {
		Map<String, RefactoringStatusWithName> mapPrograms = new HashMap<String, RefactoringStatusWithName> ();
		Scanner scanner = new Scanner(cARefactorResultsFile);
		
		StringTokenizer strTokenResult = null;
		String newName = null;
		String nomePrograma = null;
		String infoMessage = null;
		String errorMessage = null;

		while(scanner.hasNextLine()){
			
			try{
				infoMessage = null;
				errorMessage = null;
				String line = scanner.nextLine();
				StringTokenizer strToken = new StringTokenizer(line, "=");
				nomePrograma = strToken.nextToken();
				String resultExecution = strToken.nextToken();
				//linha de mensagem de erro
				if(!resultExecution.endsWith(";0")){
					errorMessage = scanner.nextLine() + scanner.nextLine();
					scanner.nextLine();
					scanner.nextLine();scanner.nextLine();
					scanner.nextLine();
					scanner.nextLine();scanner.nextLine();
					String execution = scanner.nextLine();
					StringTokenizer strTokenExec = new StringTokenizer(execution, ";");
					strTokenExec.nextToken();strTokenExec.nextToken();strTokenExec.nextToken();strTokenExec.nextToken();
					newName = strTokenExec.nextToken();
				}else{
					
					strTokenResult = new StringTokenizer(resultExecution, ";");
					strTokenResult.nextToken();strTokenResult.nextToken();strTokenResult.nextToken();
					strTokenResult.nextToken();
					String errorMessageWithChars = strTokenResult.nextToken();
					errorMessage = errorMessageWithChars.substring(errorMessageWithChars.indexOf("[")+1, errorMessageWithChars.indexOf("]"));
					
					String infoMessageWithChars = strTokenResult.nextToken();
					infoMessage = infoMessageWithChars.substring(infoMessageWithChars.indexOf("[")+1, infoMessageWithChars.indexOf("]"));
	
					strTokenResult.nextToken();
					strTokenResult.nextToken();
					strTokenResult.nextToken();strTokenResult.nextToken();
					//strTokenResult.nextToken();strTokenResult.nextToken();//strTokenResult.nextToken();
					newName = strTokenResult.nextToken();
				}
			}catch(NoSuchElementException e){
				continue;
			}
			RefactoringStatus rs = new RefactoringStatus();
			rs.addError(errorMessage==null?"":errorMessage);
			rs.addInfo(infoMessage==null?"":infoMessage);
//			System.out.println("Programa: " + nomePrograma + " newName: " + newName + " rs: " + rs.toString() );
			mapPrograms.put(nomePrograma, new RefactoringStatusWithName(newName, rs));
		}
		return mapPrograms;
	}

}

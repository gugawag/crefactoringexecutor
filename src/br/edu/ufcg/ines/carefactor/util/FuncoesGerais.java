package br.edu.ufcg.ines.carefactor.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

public class FuncoesGerais {

	private static Logger logger = Logger.getLogger(FuncoesGerais.class);
	
	public static CommandExecutionResult executaComando(String comando, String dir) {

		CommandExecutionResult commandExecutionResult = new CommandExecutionResult();
		String cmd = comando;
		Runtime run = Runtime.getRuntime();
		Process pr = null;
		try {
//			logger.info("Vai executar comando: " + cmd + " na pasta: " + dir);
			if (dir == null){
				pr = run.exec(cmd);
			} else{
				pr = run.exec(cmd, null, new File(dir));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			commandExecutionResult.setExceptionMessage(e.getMessage());
			return commandExecutionResult;
		}
		try {
			 // any error message?
            StreamGobbler errorGobbler = new 
                StreamGobbler(pr.getErrorStream(), "ERROR");

            // any output?
            StreamGobbler outputGobbler = new 
                StreamGobbler(pr.getInputStream(), "OUTPUT");
                
            // kick them off
            errorGobbler.start();
            outputGobbler.start();
                                    
            // any error???
            int exitVal = pr.waitFor();
            commandExecutionResult.setExitVal(exitVal);
            commandExecutionResult.setReturn(errorGobbler.getMessage());
            
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        Scanner scanner = new Scanner(pr.getInputStream());
        String retorno = null;

        //Tem retorno     
        if( scanner.hasNextLine() ){
       	 retorno = scanner.nextLine();
        }
        if ( retorno != null ){
        	commandExecutionResult.setReturn(retorno);
       	 //Sucesso
       	 if ( Integer.parseInt( retorno.trim() ) == 0 ){
       		logger.info("Executou corretamente!");
       	 } else{
       		logger.info("Deu problema na execu��o do comando. C�digo de retorno: " + retorno);
       	 }
        }
        return commandExecutionResult;
	}
	
	public static ITranslationUnit abreArquivo(String caminhoArquivo) {
		IPath path = new Path(caminhoArquivo);
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);

		// Create translation unit for file

		ITranslationUnit tu = (ITranslationUnit) CoreModel.getDefault().create(
				file);
		return tu;
	}

	public static void copyFilesToRegressionTests(String absolutePath, String refactoredProgramFolder) {
		System.out.println("cp " + absolutePath + "/test_regressao.c " + refactoredProgramFolder);
		CommandExecutionResult cer = executaComando("cp " + absolutePath + "/test_regressao.c " + refactoredProgramFolder, null);
		System.out.println("SA�DA DA C�PIA DOS TESTES DE REGRESSAO: " + cer.getExitVal());
		File f = new File(refactoredProgramFolder+"/test_regressao.c");
		System.out.println("TESTES EXISTEM? " + f.exists());
		
		File funcoesFile = new File("/Users/gugawag/Documents/mestrado/workspace/CAutomaticTester/resources/funcoes.h");
		if (!funcoesFile.exists()){
			executaComando("cp " + absolutePath + "/funcoes.h " + refactoredProgramFolder, null);
		}
	}

	public static RegressionTestsResult getRegressionTestsResult(String refactoredProgramFolder) {
		RegressionTestsResult result = new RegressionTestsResult();
		File regressionTestOutputFile = new File(refactoredProgramFolder + "/saida1.txt");
		
		//tests were not executed
		if (!regressionTestOutputFile.exists()){
			result.setTestsWasExecuted(false);
			return result;
		}
		
		Scanner scanner = null;
		try {
			scanner = new Scanner(new FileInputStream(regressionTestOutputFile.getAbsolutePath()));
		} catch (FileNotFoundException e) {
			result.setTestsWasExecuted(false);
			return result;
		}
		
		//if does not have a T (test) in the saida1.txt file, means some error
		boolean hasSomeResult = false;
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			logger.debug("LINHA: " + line);
			//error
			if (line.contains("Erro [")){
				logger.debug("LINHA COME�A COM ERRO: " + line);
				scanner.close();
				result.setTestsWasExecuted(true);
				result.setTestsPassed(false);
				result.setRegressionTestMessage(line);
				return result;
			}
			//has results
			if (line.startsWith("T")){
				logger.debug("LINHA COM T: " + line);
				hasSomeResult = true;
				StringTokenizer strToken = new StringTokenizer(line, "=");
				strToken.nextToken();
				String executionResult = strToken.nextToken();
				//results differ of the original results
				if ("0".equals(executionResult)){
					scanner.close();
					result.setTestsWasExecuted(true);
					result.setTestsPassed(false);
					return result;
				}
			}
		}
		scanner.close();
		if (!hasSomeResult){
			result.setTestsWasExecuted(true);
			result.setTestsPassed(false);
		}else{
			result.setTestsPassed(true);
		}
		return result;
	}


}

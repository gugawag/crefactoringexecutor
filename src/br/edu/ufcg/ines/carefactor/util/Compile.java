package br.edu.ufcg.ines.carefactor.util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;

import catester.RandoopUnitPatterns;


public class Compile {

	private static Logger logger = Logger.getLogger(Compile.class);
	


	//compile the generated program
	/**
	 * 
	 * @param programPath
	 * @param needAMain If it necessary a file with a main function to compile this program
	 * @return
	 */
	public static CommandExecutionResult run(String programPath, boolean needAMain) {
		File scriptPath = new File(programPath + "/compile.sh");
		File funcoesH = new File(programPath + "/funcoes.h");
		File regressionTestsFile = new File(programPath + "/test_regressao.c");
		
		if (!scriptPath.exists()){
			FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/compile.sh "
							+ programPath, null);
		}
		
		if (!funcoesH.exists()){
			FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/CAutomaticTester/resources/funcoes.h "
					+ programPath, null);

		}
		
		//cannot execute a main with regression tests file in the directory because "main" function duplication
		if (needAMain && regressionTestsFile.exists()){
			FuncoesGerais.executaComando("rm " + programPath + "/test_regressao.c ", null);
		}

		//copying main.c to be possible compile a program
		if (needAMain){
			FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/main.c "
				+ programPath, null);
		}		
		
		CommandExecutionResult commandExecutionResult = FuncoesGerais.executaComando(scriptPath.getAbsolutePath()
				+ " a.out " + programPath + " " + "c99" + " " + "clang" + " " + " ", null);

		//removing files
		FuncoesGerais.executaComando("rm " + programPath + "/compile.sh ", null);
		if (needAMain){
			FuncoesGerais.executaComando("rm " + programPath + "/main.c ", null);
		}

        return commandExecutionResult;        
	}
	
	/**
	 * 
	 * @param programPath
	 * @param needAMain If it necessary a file with a main function to compile this program
	 * @return
	 */
	public static RegressionTestsResult compileAndRunRegressionTests(String programPath) {
		File scriptPath = new File(programPath + "/compile.sh");
		File funcoesH = new File(programPath + "/funcoes.h");
		File regressionTestsFile = new File(programPath + "/test_regressao.c");
		
		if (!scriptPath.exists()){
			FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/compile.sh "
							+ programPath, null);
		}
		
		if (!funcoesH.exists()){
			FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/CAutomaticTester/resources/funcoes.h "
					+ programPath, null);

		}

		RegressionTestsResult regressionTR = null;
		File mainFile = new File(programPath + "/main.c");
		if (mainFile.exists()){
			mainFile.delete();
		}
			Scanner scanner = null;
			try {
				scanner = new Scanner(regressionTestsFile);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			Map<String, String> functionCallsMap = new HashMap<String, String>();
			StringBuilder regressionTestSource = new StringBuilder();
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if (line.contains("int main(void)")){
					//renaming main function in test_regressao.c file. 
					regressionTestSource.append(line.replace("main", "_main")+ "\n");	
				} else{
					regressionTestSource.append(line+ "\n");
				}
				if (line.contains("void test_")){
					String finalLine = line.replace("void ", "").replace("(void){", "();");
					functionCallsMap.put(finalLine, line.replace("{", ";"));
				}
			}
			
		
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(regressionTestsFile);
		        fos.write(regressionTestSource.toString().getBytes());
		        fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//scanning all the functions, generating specific main files
			RandoopUnitPatterns unitPatterns = new RandoopUnitPatterns();
			StringBuilder mainBuilder = new StringBuilder();
			
			for (String functionCall: functionCallsMap.keySet()){
				mainBuilder.append(getHeaderMain());
				mainBuilder.append(functionCallsMap.get(functionCall) + "\n");
				mainBuilder.append(unitPatterns.getPadraoMain().replace("[nome_arquivo_oraculo]",
						"oraculo1.txt").replace("[nome_arquivo_saida]", "saida1.txt") + "\n");
				mainBuilder.append(functionCall + "\n}");
				try{
					FileOutputStream fosMain = new FileOutputStream(mainFile);
					fosMain.write(mainBuilder.toString().getBytes());
					fosMain.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				CommandExecutionResult commandExecutionResult = FuncoesGerais.executaComando(scriptPath.getAbsolutePath()
						+ " a.out " + programPath + " " + "c99" + " " + "clang" + " " + " ", null);
				if (commandExecutionResult.getExitVal()!=0){
					return new RegressionTestsResult(false, false, null);
				}
				CommandExecutionResult regressionExecutionResult = FuncoesGerais.executaComando(programPath + 
						"execute.sh a.out " + programPath, null);

				regressionTR = FuncoesGerais.getRegressionTestsResult(programPath);
				if (!regressionTR.isTestsPassed()){
					return regressionTR;
				}
				mainBuilder = new StringBuilder();
			}

		

		//removing files
			if(mainFile.exists()){
				mainFile.delete();
			}
		FuncoesGerais.executaComando("rm " + programPath + "/compile.sh ", null);

        return regressionTR;        
	}

	private static StringBuilder getHeaderMain() {
		StringBuilder mainBuilder = new StringBuilder();
		//inserting required functions
		mainBuilder.append("#include <stdio.h>\n");
		mainBuilder.append("#include <stdlib.h>\n");
		mainBuilder.append("#include <string.h>\n");
		mainBuilder.append("#include <stdbool.h>\n");
		mainBuilder.append("extern FILE *entrada, *saida, *oraculo;");
		
		mainBuilder.append("static void signal_handler( int sig ) {\n");
		mainBuilder.append("printf(\"Erro [%d]\", sig);\n");
		mainBuilder.append("fflush(stdout);\n");
		mainBuilder.append("fflush(stderr);\n");
		mainBuilder.append("psignal( sig, \"Tipo: \");\n");
		mainBuilder.append("signal( sig, signal_handler);\n");
		mainBuilder.append("fflush(stdout);\n");
		mainBuilder.append("fflush(stderr);\n");
		mainBuilder.append("exit(sig);}\n");
		

		mainBuilder.append("static void cadastraSinais(){\n");
		mainBuilder.append("signal( SIGFPE, signal_handler );\n");
		mainBuilder.append("signal( SIGSEGV, signal_handler );\n");
		mainBuilder.append("signal( SIGILL, signal_handler );\n");
		mainBuilder.append("signal( SIGABRT, signal_handler );\n");
		mainBuilder.append("signal( SIGHUP, signal_handler );\n");
		mainBuilder.append("signal( SIGINT, signal_handler );\n");
		mainBuilder.append("signal( SIGQUIT, signal_handler );\n");
		mainBuilder.append("signal( SIGTRAP, signal_handler );\n");
		mainBuilder.append("signal( SIGIOT, signal_handler );\n");
		mainBuilder.append("signal( SIGKILL, signal_handler );\n");
		mainBuilder.append("signal( SIGBUS, signal_handler );\n");
		mainBuilder.append("signal( SIGSYS, signal_handler );\n");
		mainBuilder.append("signal( SIGPIPE, signal_handler );\n");
		mainBuilder.append("signal( SIGALRM, signal_handler );\n");
		mainBuilder.append("signal( SIGTERM, signal_handler );\n");
		mainBuilder.append("signal( SIGURG, signal_handler );\n");
		mainBuilder.append("signal( SIGSTOP, signal_handler );\n");
		mainBuilder.append("signal( SIGTSTP, signal_handler );\n");
		mainBuilder.append("signal( SIGCONT, signal_handler );\n");
		mainBuilder.append("signal( SIGCHLD, signal_handler );\n");
		mainBuilder.append("signal( SIGTTIN, signal_handler );\n");
		mainBuilder.append("signal( SIGTTOU, signal_handler );\n");
		mainBuilder.append("signal( SIGIO, signal_handler );\n");
		mainBuilder.append("signal( SIGXCPU, signal_handler );\n");
		mainBuilder.append("signal( SIGXFSZ, signal_handler );\n");
		mainBuilder.append("signal( SIGVTALRM, signal_handler );\n");
		mainBuilder.append("signal( SIGPROF, signal_handler );\n");
		mainBuilder.append("signal( SIGWINCH, signal_handler );\n");
		mainBuilder.append("signal( SIGUSR1, signal_handler );\n");
		mainBuilder.append("signal( SIGUSR2, signal_handler );}\n");

		return mainBuilder;
	}


}

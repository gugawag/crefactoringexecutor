package br.edu.ufcg.ines.carefactor.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class AppResultsAnalyserTest {

	@Test
	public void testRemoveSpecificValue() {
		String text = "B.c:5:9: error: redefinition of 'argc'";
		String expectedText = "B.c:5:9: error: redefinition of 'x'";
		assertTrue(expectedText.equals(AppResultsAnalyser.removeSpecificValues(text)));
		
		text = "B.c:5:9: error: redefinition of '4argc12'";
		assertTrue(expectedText.equals(AppResultsAnalyser.removeSpecificValues(text)));
		
		text = "B.c:5:9: error: redefinition of '2'";
		assertTrue(expectedText.equals(AppResultsAnalyser.removeSpecificValues(text)));
		
		text = "use of undeclared identifier 'LocalVar_1'; did you mean 'LocalVar_0'?";
		expectedText="use of undeclared identifier 'x'; did you mean 'x'?";
		assertTrue(expectedText.equals(AppResultsAnalyser.removeSpecificValues(text)));
	}

}

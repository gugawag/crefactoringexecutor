package br.edu.ufcg.ines.carefactor.util;

public class CommandExecutionResult {
	
	private int exitVal;
	private String _return;
	private String exceptionMessage;
	
	
	public CommandExecutionResult() {
		super();
	}

	public CommandExecutionResult(int exitVal, String _return,
			String exceptionMessage) {
		super();
		this.exitVal = exitVal;
		this._return = _return;
		this.exceptionMessage = exceptionMessage;
	}
	
	public int getExitVal() {
		return exitVal;
	}
	public void setExitVal(int exitVal) {
		this.exitVal = exitVal;
	}
	public String getReturn() {
		return _return;
	}
	public void setReturn(String _return) {
		this._return = _return;
	}
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	
	public boolean isCompiled(){
		return exitVal==0;
	}
		
	public String toString(){
		return "COMMAND_RETURN [" + _return + "];COMMAND_EXIT_VAL [" + exitVal + "];COMMAND_EXCEPTION_MESSAGE[" + exceptionMessage + "]";  
	}


}

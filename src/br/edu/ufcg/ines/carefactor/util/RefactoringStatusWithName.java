package br.edu.ufcg.ines.carefactor.util;

import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public class RefactoringStatusWithName {
	
	private String name;
	private RefactoringStatus refactoringStatus;
	
	
	public RefactoringStatusWithName(String name,
			RefactoringStatus refactoringStatus) {
		super();
		this.name = name;
		this.refactoringStatus = refactoringStatus;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public RefactoringStatus getRefactoringStatus() {
		return refactoringStatus;
	}
	public void setRefactoringStatus(RefactoringStatus refactoringStatus) {
		this.refactoringStatus = refactoringStatus;
	}
	
	

}
